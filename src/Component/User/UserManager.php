<?php

declare(strict_types=1);

namespace App\Component\User;

use App\Component\Core\AbstractManager;
use App\Entity\User;

/**
 * Class UserManager
 *
 * @method save(User $entity, bool $needToFlush = false) : void
 * @package App\Component\User
 */
class UserManager extends AbstractManager
{
}
