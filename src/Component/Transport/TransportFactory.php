<?php

namespace App\Component\Transport;

use App\Entity\MediaObject;
use App\Entity\Transport;

class TransportFactory
{
    public function create(string $type, string $serialNumber, string $stateNumber, MediaObject $mediaObject): Transport
    {
        return (new Transport())
            ->setType($type)
            ->setSerialNumber($serialNumber)
            ->setStateNumber($stateNumber)
            ->setCarImage($mediaObject);
    }
}
