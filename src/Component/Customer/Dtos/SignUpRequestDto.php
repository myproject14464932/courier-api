<?php

namespace App\Component\Customer\Dtos;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class SignUpRequestDto
{
    public function __construct(
        #[Assert\Email, Assert\NotBlank]
        #[Groups(['customer:write'])]
        private string $email,

        #[Groups(['customer:write'])]
        #[Assert\Length(min: 7)]
        private string $password,

        #[Groups(['customer:write'])]
        #[Assert\NotBlank]
        private string $givenName,

        #[Groups(['customer:write'])]
        private string $familyName,

        #[Groups(['customer:write'])]
        #[Assert\Length(min: 9)]
        private string $phone,
    ) {
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getGivenName(): string
    {
        return $this->givenName;
    }

    public function getFamilyName(): string
    {
        return $this->familyName;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }
}