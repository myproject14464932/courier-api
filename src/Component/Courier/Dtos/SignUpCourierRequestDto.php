<?php

namespace App\Component\Courier\Dtos;

use App\Entity\MediaObject;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class SignUpCourierRequestDto
{

    public function __construct(
        #[Groups(['courier:write'])]
        #[Assert\NotBlank]
        private string $givenName,

        #[Groups(['courier:write'])]
        private string $familyName,

        #[Groups(['courier:write'])]
        private MediaObject $image,

        #[Groups(['courier:write'])]
        #[Assert\Length(min: 9)]
        private string $phone,

        #[Groups(['courier:write'])]
        private string $email,

        #[Groups(['courier:write'])]
        #[Assert\Length(min: 7)]
        private string $password,

        #[Groups(['courier:write'])]
        #[Assert\NotBlank]
        private string $type,

        #[Groups(['courier:write'])]
        #[Assert\NotBlank]
        private string $serialNumber,

        #[Groups(['courier:write'])]
        #[Assert\NotBlank]
        private string $stateNumber,

        #[Groups(['courier:write'])]
        private MediaObject $carImage,

    ) {
    }

    public function getGivenName(): string
    {
        return $this->givenName;
    }

    public function getFamilyName(): string
    {
        return $this->familyName;
    }

    public function getImage(): MediaObject
    {
        return $this->image;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getSerialNumber(): string
    {
        return $this->serialNumber;
    }

    public function getStateNumber(): string
    {
        return $this->stateNumber;
    }

    public function getCarImage(): MediaObject
    {
        return $this->carImage;
    }
}
