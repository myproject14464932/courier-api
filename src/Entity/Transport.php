<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\DeleteAction;
use App\Repository\TransportRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: TransportRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get' => [
            'security' => "is_granted('ROLE_USER')",
            'normalization_context' => ['groups' => ['transport:read']],
        ],
//                'post'               => [
//            'controller' => ''
//        ],
    ],
    itemOperations: [
        'get'            => [],
        'put' => [
            'security' => "object.getId() == user || is_granted('ROLE_ADMIN')",
            'denormalization_context' => ['groups' => ['transport:put:write']],
        ],
        'delete' => [
            'controller' => DeleteAction::class,
            'security' => "object.getId() == user || is_granted('ROLE_ADMIN')",
        ],
    ],
    denormalizationContext: ['groups' => ['transport:write']],
    normalizationContext: ['groups' => ['transport:read', 'transports:read']],
)]
class Transport
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['transport:read', 'transport:put:write', 'courier:read'])]
    private $type;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['transport:read', 'transport:put:write', 'courier:read'])]
    private $serialNumber;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['transport:read', 'transport:put:write', 'courier:read'])]
    private $stateNumber;

    #[ORM\OneToOne(targetEntity: MediaObject::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['transport:read', 'transport:put:write', 'courier:read'])]
    private $carImage;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSerialNumber(): ?string
    {
        return $this->serialNumber;
    }

    public function setSerialNumber(string $serialNumber): self
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    public function getStateNumber(): ?string
    {
        return $this->stateNumber;
    }

    public function setStateNumber(string $stateNumber): self
    {
        $this->stateNumber = $stateNumber;

        return $this;
    }

    public function getCarImage(): ?MediaObject
    {
        return $this->carImage;
    }

    public function setCarImage(MediaObject $carImage): self
    {
        $this->carImage = $carImage;

        return $this;
    }
}
