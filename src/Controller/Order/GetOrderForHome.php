<?php
declare(strict_types=1);

namespace App\Controller\Order;

use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GetOrderForHome extends AbstractController
{
    public function __invoke(OrderRepository $orderRepository): array
    {
        return $orderRepository->findById(1);
    }
}