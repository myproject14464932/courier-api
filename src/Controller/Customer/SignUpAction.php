<?php

namespace App\Controller\Customer;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Component\Customer\CustomerFactory;
use App\Component\Customer\Dtos\SignUpRequestDto;
use App\Component\User\UserFactory;
use App\Component\User\UserManager;
use App\Entity\Customer;
use App\Repository\CustomerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class SignUpAction extends AbstractController
{
    public function __invoke(
        UserFactory $userFactory,
        Request $request,
        UserManager $userManager,
        CustomerFactory $customerFactory,
        CustomerRepository $customerRepository,
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ): Customer {
        /** @var SignUpRequestDto $signUpRequest */
        $signUpRequest = $serializer->deserialize($request->getContent(), SignUpRequestDto::class, 'json');
        $user = $userFactory->create($signUpRequest->getEmail(), $signUpRequest->getPassword());

        $validator->validate($user);

        $customer = $customerFactory->create(
            $user,
            $signUpRequest->getGivenName(),
            $signUpRequest->getFamilyName(),
            $signUpRequest->getPhone()
        );

        $validator->validate($signUpRequest);

        $userManager->save($user);
        $customerRepository->add($customer, true);

        return $customer;
    }
}
