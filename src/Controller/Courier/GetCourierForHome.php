<?php
declare(strict_types=1);

namespace App\Controller\Courier;

use App\Repository\CourierRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GetCourierForHome extends AbstractController
{
    public function __invoke(CourierRepository $courierRepository): array
    {
        return $courierRepository->findById(1);
    }
}