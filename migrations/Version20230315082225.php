<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230315082225 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add property Courier';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE courier ADD image_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE courier ADD CONSTRAINT FK_CF134C7C3DA5256D FOREIGN KEY (image_id) REFERENCES media_object (id)');
        $this->addSql('CREATE INDEX IDX_CF134C7C3DA5256D ON courier (image_id)');
        $this->addSql('ALTER TABLE media_object DROP FOREIGN KEY FK_14D43132E3D8151C');
        $this->addSql('DROP INDEX IDX_14D43132E3D8151C ON media_object');
        $this->addSql('ALTER TABLE media_object DROP courier_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE courier DROP FOREIGN KEY FK_CF134C7C3DA5256D');
        $this->addSql('DROP INDEX IDX_CF134C7C3DA5256D ON courier');
        $this->addSql('ALTER TABLE courier DROP image_id');
        $this->addSql('ALTER TABLE media_object ADD courier_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE media_object ADD CONSTRAINT FK_14D43132E3D8151C FOREIGN KEY (courier_id) REFERENCES courier (id)');
        $this->addSql('CREATE INDEX IDX_14D43132E3D8151C ON media_object (courier_id)');
    }
}
