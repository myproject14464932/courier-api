<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230113150149 extends AbstractMigration
{
    public function getDescription(): string
    {
        return ' Creat Transport Entity';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE transport (id INT AUTO_INCREMENT NOT NULL, car_image_id INT NOT NULL, type VARCHAR(255) NOT NULL, serial_number VARCHAR(255) NOT NULL, state_number VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_66AB212EB2931069 (car_image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE transport ADD CONSTRAINT FK_66AB212EB2931069 FOREIGN KEY (car_image_id) REFERENCES media_object (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE transport');
    }
}
